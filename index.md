# Bughunting competition

Show the world that you are a great hacker! Come and compete with others. This year, it’s virtual.

Bughunting is a competition in which you fix bugs in various applications. It imitates part of the workday of a Linux distribution developer. We prepared for you a couple of tasks in various programming languages. Available languages include C, Python, Perl, Shell, JavaScript, and Java. We hope that anyone can find something he/she knows and likes.

The purpose of every task is to find the bug and fix it in the code. Your solution is evaluated in real-time after you submit it and you score points if your solution is correct. Some of the issues are artificial and were created only for the competition, while others are real-life issues.

If you like this kind of puzzles and would like to do this for the living, talk to our Talent acquisition team that is available during the Open House event on <http://bit.ly/openhouse-ta>.

## Competition organization

Competition is organized into 50 minutes long sessions. After each session, there is a 10 minutes break. Sessions consist of tasks with various difficulty. More difficult tasks are for more points. We advise you to start with the easier ones and once you gain confidence, you can pick more advanced tasks. So don’t worry, you will definitely find something that will work for you.

## Rules

* **Registration is required before the competition via this form: <https://forms.gle/UsJNQmV1oRSfjyKEA>**
* After the registration, you get a e-mail with instructions on how to set up your environment.
* Main event is available via BlueJeans Primetime at <https://primetime.bluejeans.com/a2m/register/tguszkeb>
* Please, connect to the BlueJeans Primetime no later than 10 minutes before your session starts.
* You can attend the competition only once!
* Solutions can be dirty, they just must not break the expected functionality of the program.
* Tasks have **hints** to help you find the issue. Each time you uncover a hint, the system lowers the maximum number of points you can get for the task..
* The solution can also be a dirty fix, but you must not break the basic functionality of the application. Feel free to test how good our tests are for submitted solutions ;)
* If more people score the same points, the one who was the first is preferred in the ranking
* Winners are announced at the end of each round.

## Setting up the environment

* A quick (6min long) intro how the competition works is available at <https://youtu.be/v5pW8wWUNRk>.
* Have you any troubles with setting up the environment, reach out to us at <https://meet.google.com/suj-qhzy-pkw>.
* Linux containers are used for the competition environment, therefore you need to install the container runtime.
* Please, **prepare the environment ahead**. Pulling the container image takes some time, and you can try working with the container before the competition, so you can focus on the tasks during the competititon itself.
* We recommend to use `podman`, but it should work fine with `docker` as well.

1. Depending on the operating system you use, install the `podman` or `docker`:
   * <https://docs.docker.com/get-docker>
   * <https://podman.io/getting-started/installation.html> 
     * For the `docker` variant, run the docker daemon (command for Fedora below; for other OS, see the documentation):
    ```console
    sudo systemctl start docker
    ```

   * We use docker command below, but if you didn’t install podman-docker sub-package (which adds a symlink `docker` -> `podman`), then it should simply work if you replace `docker` with `podman` in all the commands below
2. Prepare an empty working directory
    ```console
    mkdir bughunting-tasks
    ```

3. Now, you can go to <http://www.bughunting.cz> which will require signing up and will show you task list and their descriptions. It will also show you specific command to run the container for your unique case.

4. Pull and run the container in background and bind-mount the `bughunting-tasks` directory inside. For testing, use the key `test` instead of '???'. For the competition, use a key provided in the web interface after signing up and use it instead of '????':
    ```console
    docker pull quay.io/bughunting/client
    docker run -e KEY=???? -d -v $(pwd)/bughunting-tasks:/home/bughunting/sources:z,rw --name bughunting quay.io/bughunting/client
    ```
    Pulling the image may take some time (minutes) for the first time. Any further run will be faster.
    

5. Connect to the running container:
    ```console
    docker exec -ti bughunting bash
    ```

## Solve tasks

Now, the following commands are run inside the container:

6. You can list the directories with tasks:
    ```
    [bughunting] $ ls
    warm_up
    ```

7. For working on a task, you need to be inside the task directory
    ```
    [bughunting] $ cd warm_up
    ```

8. Edit the source code

   You can edit the file either inside the container (Press CTRL+X to exit the `nano` editor):
    ```
    [bughunting] $ nano script.sh
    ```

   Or you can edit the file in your `bughunting-tasks` directory on your host (this does not work with `docker` without further configuration, it works by default with `podman`)

9. Once happy with the solution, submit the task for evaluation:
    ```
    [bughunting] $ hunt
    ```

10. You see immediately whether you scored or not
    ```
    OK: You gain 1 score points.
    ```

## Where are the tasks

As seen in the example above, in the container, the directory **/home/bughunting** contains a directory named **sources**. This directory contains directories with names of the tasks. When you open the directory with one of the task names, you’ll see all the sources. **Follow the steps to reproduce a failure that is described on the website.**

Please note that the tasks are usually artificial and the solutions are pretty easy. Don’t spend too much time on one task - if you struggle, there are other tasks that can be a better fit for you. Also consider using task hints if you are stuck.

## How to submit your fix

To submit the task for verification, run the command `hunt` in the terminal, inside the task directory in the container. You can submit every task as many times as you want.


## F&Q

1. **Question: I want to start the container and see the following error, what should I do?**

       Error: error creating container storage: the container name "bughunting"
       is already in use by "63e4f8f69068f2121ab7b6ec261b7cb89c51fd68c6b3cc462786d767bcd2dee5".
       You have to remove that container to be able to reuse that name.: that
       name is already in use

    **Answer:** You already have created a container before. Use `docker -ti exec bughunting bash` to attach to that existing container.

1. **Question: I'm getting the following error when I'm pulling the image, what should I do?**

       $ docker run -e KEY=... -d --pull always --name bughunting quay.io/bughunting/client
       Emulate Docker CLI using podman. Create /etc/containers/nodocker to quiet msg.
       Trying to pull quay.io/bughunting/client...
         Get https://d3uo42mtx6z2cr.cloudfront.net/sha256/cd/cd4916bc5314ba8615a141133fbe9872dfe2e1dcfbab66e6e2ec34f5ba298121?Expires=1586934124&Signature=bqPl9sRMYa8I3FsI8oe20LDsU8p6jtadMfKS~RiyXoY98IjlA~tf0DPMnJokpApMuu91L14Gg0iBn9yiXVtFXzaJlJNRpf75~ak3MdG1MyTGRLOUK~MM34A1ZVe6mTYZ9sb4cnsEFRr8min5S6isTgQ6q4rRJFF2qJcB1cHJ2YD6s5h0plcrXvYgIwmzSwluutgkIwOpABxVt-ykTVpTt8~HHcBEKBr-FQaZuwHPPJSGbfGwUtMGVgTLb3Pl7hlf-Y~KSf2cvS7sQepxRrZ5rgYtTYMFtlw08eZ3o8yKEgLODSh0D54KRCBOiz3dgkwI9J-gC5igPXB-JBnJWu-qxw__&Key-Pair-Id=APKAJ67PQLWGCSP66DGA: dial tcp: lookup d3uo42mtx6z2cr.cloudfront.net on 127.0.0.1:53: server misbehaving
       Error: unable to pull quay.io/bughunting/client: unable to pull image: Error parsing image configuration: Get https://d3uo42mtx6z2cr.cloudfront.net/sha256/cd/cd4916bc5314ba8615a141133fbe9872dfe2e1dcfbab66e6e2ec34f5ba298121?Expires=1586934124&Signature=bqPl9sRMYa8I3FsI8oe20LDsU8p6jtadMfKS~RiyXoY98IjlA~tf0DPMnJokpApMuu91L14Gg0iBn9yiXVtFXzaJlJNRpf75~ak3MdG1MyTGRLOUK~MM34A1ZVe6mTYZ9sb4cnsEFRr8min5S6isTgQ6q4rRJFF2qJcB1cHJ2YD6s5h0plcrXvYgIwmzSwluutgkIwOpABxVt-ykTVpTt8~HHcBEKBr-FQaZuwHPPJSGbfGwUtMGVgTLb3Pl7hlf-Y~KSf2cvS7sQepxRrZ5rgYtTYMFtlw08eZ3o8yKEgLODSh0D54KRCBOiz3dgkwI9J-gC5igPXB-JBnJWu-qxw__&Key-Pair-Id=APKAJ67PQLWGCSP66DGA: dial tcp: lookup d3uo42mtx6z2cr.cloudfront.net on 127.0.0.1:53: server misbehaving

    **Answer:** From time to time, there are some issues with quay.io. Just keep trying and the command will eventually succeed.

1. **Question: I ran the container with a wrong key. What should I do?**

    **Answer:** Kill and remove the container by `docker kill bughunting ; docker rm bughunting`. Then, run it again with a correct key.


1. **Question: I would like to use gdb (or any other tool for debugging). What should I do?**

    **Answer:** You can install further packages in the container. For example, gdb can be installed using `dnf install gdb`.

